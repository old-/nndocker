#! /bin/sh

# don't log or write anything by using:
# logfile="/dev/null"
# or touch logfile; logfile="~/logfile"
# and appending to all lines >>$logfile 2>&1
# Made by graham a for softlayer server using nvidia grid k2 gpu to do neural net stuff
################################################################################
# basic setup using 14.04 to create GPU enabled docker containers to train model using Keras, need to setup initial models
echo -e "\033[0;30;42m fresh install \033[0m\033[0;32m\033[0m"

apt-get update && apt-get install -y curl wget git htop glances vim software-properties-common

# tmux 2.1
add-apt-repository -y ppa:pi-rho/dev
apt-get update && apt-get install -y tmux-next
ln -s /usr/bin/tmux-next /usr/local/bin/tmux

################################################################################
# docker stuff
echo -e "\033[0;30;43m installing docker from official script \033[0m\033[0;33m\033[0m"
curl -sSL https://get.docker.com/ | sh

# Docker Compose
curl -L https://github.com/docker/compose/releases/download/1.5.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

################################################################################
# install cuda 7.5
# -------------------
# NOTES:
# Need CORRECT DRIVER, not sure if I need cuda 7.5 thing based on
# https://github.com/NVIDIA/nvidia-docker but for some reason i believe it didnt work without that
# -------------------
# OFFICIAL DOCUMENTATION:
# http://developer.download.nvidia.com/compute/cuda/7.5/Prod/docs/sidebar/CUDA_Quick_Start_Guide.pdf
# http://developer.download.nvidia.com/compute/cuda/7.5/Prod/docs/sidebar/CUDA_Installation_Guide_Linux.pdf
############################
# POSSIBLY MAY NEED:
# sudo apt-get install ubuntu-drivers-common
# sudo ubuntu-drivers autoinstall
# cd /usr/local/cuda/samples/1_Utilities/deviceQuery
# make
# ./deviceQuery
############################
#
echo -e "\033[0;30;43m installing nvidia driver \033[0m\033[0;33m\033[0m"
add-apt-repository ppa:graphics-drivers/ppa -y
apt-get install -y nvidia-361 nvidia-settings


# echo -e "\033[0;30;43m installing nvidia cuda 7.5 \033[0m\033[0;33m\033[0m"
# wget http://developer.download.nvidia.com/compute/cuda/7.5/Prod/local_installers/cuda-repo-ubuntu1404-7-5-local_7.5-18_amd64.deb
# sudo dpkg -i cuda-repo-ubuntu1404-7-5-local_7.5-18_amd64.deb
# apt-get update && apt-get install -y cuda nvidia-current

### #NVIDIA NOTE if you install nvidia stuff on host then need to export to bash
# echo 'export CUDA_HOME=/usr/local/cuda-7.5' >> /root/.bashrc
# echo 'export LD_LIBRARY_PATH=${CUDA_HOME}/lib64' >> /root/.bashrc
# echo 'export PATH=${CUDA_HOME}/bin:${PATH}' >> /root/.bashrc


# if you install with runfile:
# http://us.download.nvidia.com/XFree86/Linux-x86_64/352.63/NVIDIA-Linux-x86_64-352.63.run

################################################################################
# Final Stuff
# -------------------

mkdir /root/code/ && cd /root/code/
git clone https://gitlab.com/grahamannett/nndocker.git
git clone https://gitlab.com/grahamannett/dockerbuilds.git
cp nndocker/tmux.conf /root/.tmux-next.conf
cp nndocker/bashrc /root/.bashrc
cp nndocker/bash_aliases /root/.bash_aliases

git clone https://github.com/NVIDIA/nvidia-docker
cd nvidia-docker && make install
nvidia-docker volume setup

nvidia-docker run nvidia/cuda nvidia-smi


# allow sshfs

sudo gpasswd -a $USER fuse

################################################################################
# may need to set up another user aswell so not strictly using root
# adduser graham sudo

echo -e "\033[0;30;42m all done \033[0m\033[0;32m\033[0m"
