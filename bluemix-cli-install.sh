#! /bin/sh
# graham annett - 5/24/2016
# install bluemix and cf cli

curl -L "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" | tar -zx
mv cf /usr/local/bin/cf

curl -L "http://public.dhe.ibm.com/cloud/bluemix/cli/bluemix-cli/Bluemix_CLI_0.3.3_amd64.tar.gz" | tar -zx
cd Bluemix_CLI && ./install_bluemix_cli && cd ..

cf install-plugin -f https://static-ice.ng.bluemix.net/ibm-containers-linux_x64
