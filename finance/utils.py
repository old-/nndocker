import numpy as np


class FinanceTimeSeries(object):
    def __init__(self,
                 step_size_X,
                 step_size_y,
                 columns=['Open', 'Close']):
        self.mean = None
        self.std = None

    def flow(self, X, y, batch_size=32, shuffle=False, seed=None,
             save_to_dir=None, save_prefix="", save_format="jpeg"):
        assert len(X) == len(y)
        self.X = X
        self.y = y
        self.save_to_dir = save_to_dir
        self.save_prefix = save_prefix
        self.save_format = save_format
        self.flow_generator = self._flow_index(
            X.shape[0], batch_size, shuffle, seed)
        return self

    # def __next__(self):
        # return self.next()


def df_rolling_window(df, x_window, y_size):
    df = df.transpose().values
    shape = df.shape[:-1] + (df.shape[-1] - x_window + 1, x_window)
    strides = df.strides + (df.strides[-1],)
    return np.lib.stride_tricks.as_strided(df, shape=shape, strides=strides).transpose()


def finance_gen(df, step_size_x, step_size_y):
    df = df.values
